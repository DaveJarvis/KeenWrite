#!/usr/bin/env bash

readonly FONTS_DIR="/usr/local/share/fonts"
readonly DOWNLOAD_DIR=$(mktemp -d)

cleanup() {
  if [ -d "${DOWNLOAD_DIR}" ]; then
    rm -rf "${DOWNLOAD_DIR}"
  fi
}

trap cleanup EXIT

if [ ! -d "${FONTS_DIR}" ]; then
  echo "ERROR: Create ${FONTS_DIR} and ensure write access."
  exit 1
fi

while IFS=',' read -r url extension; do
  [[ -n "$url" ]] || continue

  filename=$(basename "${url}")

  if [ ! -d "${FONTS_DIR}/${extension}" ]; then
    echo "ERROR: Create ${FONTS_DIR}/${extension} and ensure write access."
    exit 1
  fi

  echo "Downloading ${url} to ${DOWNLOAD_DIR}"
  wget --quiet -P "${DOWNLOAD_DIR}" "${url}"

  font_dir="${FONTS_DIR}/${extension}"

  echo "Extracting ${extension} to ${font_dir}"
  unzip -j -o -d "${font_dir}" "${DOWNLOAD_DIR}/${filename}" "*.${extension}"
done < urls.csv

