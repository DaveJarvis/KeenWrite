package com.keenwrite.processors.markdown.extensions;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.parser.Parser.ParserExtension;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance( TestInstance.Lifecycle.PER_CLASS )
public abstract class ExtensionTest {
  private final List<ParserExtension> mExtensions = new LinkedList<>();

  @ParameterizedTest
  @MethodSource( "getDocuments" )
  public void test_Extensions_Markdown_Html(
    final String input, final String expected
  ) {
    final var pBuilder = Parser.builder();
    final var hBuilder = HtmlRenderer.builder();
    final var parser = pBuilder.extensions( mExtensions ).build();
    final var renderer = hBuilder.extensions( mExtensions ).build();

    final var document = parser.parse( input );
    final var actual = renderer.render( document );

    assertEquals( expected, actual );
  }

  protected void addExtension( final ParserExtension extension ) {
    mExtensions.add( extension );
  }

  protected Arguments args( final String in, final String out ) {
    return Arguments.of( in, out );
  }

  protected abstract Stream<Arguments> getDocuments();
}
