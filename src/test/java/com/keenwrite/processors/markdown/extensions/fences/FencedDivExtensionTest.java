/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences;

import com.keenwrite.processors.markdown.extensions.ExtensionTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class FencedDivExtensionTest extends ExtensionTest {

  @BeforeAll
  protected void setup() {
    addExtension( FencedDivExtension.create() );
  }

  @Override
  protected Stream<Arguments> getDocuments() {
    return Stream.of(
      args(
        """
          >
          > ::: {.concurrent title="3:58"}
          > Line 1
          > :::
          >
          > ::: {.concurrent title="3:59"}
          > Line 2
          > :::
          >
          > ::: {.concurrent title="4:00"}
          > Line 3
          > :::
          >
          """,
        """
          <blockquote>
          <div class="concurrent" data-title="3:58">
          <p>Line 1</p>
          </div><div class="concurrent" data-title="3:59">
          <p>Line 2</p>
          </div><div class="concurrent" data-title="4:00">
          <p>Line 3</p>
          </div>
          </blockquote>
          """
      ),
      args(
        """
          > Hello
          >
          > ::: world
          > Adventures
          >
          > in **absolute**
          >
          > nesting.
          > :::
          >
          > Goodbye
          """,
        """
          <blockquote>
          <p>Hello</p>
          <div class="world">
          <p>Adventures</p>
          <p>in <strong>absolute</strong></p>
          <p>nesting.</p>
          </div>
          <p>Goodbye</p>
          </blockquote>
          """
      )
    );
  }
}
