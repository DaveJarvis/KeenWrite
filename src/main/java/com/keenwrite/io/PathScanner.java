package com.keenwrite.io;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.keenwrite.util.Strings.sanitize;
import static java.lang.System.getenv;
import static java.nio.file.Files.isExecutable;
import static java.util.Arrays.asList;

/**
 * Responsible for finding the fully qualified PATH to an executable file.
 */
public class PathScanner {
  /**
   * For finding executable programs. These are used in an O( n^2 ) search,
   * so don't add more entries than necessary.
   */
  private static final String[] EXECUTABLE_EXTENSIONS = {
    "", ".exe", ".bat", ".cmd", ".com", ".msc", ".msi",
  };

  public static List<String> scan() {
    final var path = sanitize( getenv( "PATH" ) );
    final var directories = path.split( File.pathSeparator );

    return new ArrayList<>( asList( directories ) );
  }

  public static Optional<Path> scanExtensions(
    final String directory,
    final String executable
  ) {
    for( final var ext : EXECUTABLE_EXTENSIONS ) {
      final var dir = Path.of( directory );
      final var path = dir.resolve( executable + ext );

      if( isExecutable( path ) ) {
        return Optional.of( path );
      }
    }

    return Optional.empty();
  }
}
