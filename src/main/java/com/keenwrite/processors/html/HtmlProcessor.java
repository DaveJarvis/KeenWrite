/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.html;

import com.keenwrite.processors.ExecutorProcessor;
import com.keenwrite.processors.Processor;
import com.keenwrite.processors.ProcessorContext;
import com.whitemagicsoftware.keenquotes.parser.Curler;

import static com.keenwrite.processors.html.Configuration.createCurler;
import static com.whitemagicsoftware.keenquotes.lex.FilterType.FILTER_XML;
import static com.whitemagicsoftware.keenquotes.parser.Apostrophe.CONVERT_RSQUOTE_HEX;

/**
 * This is the processor used when an HTML file name extension is encountered.
 */
public final class HtmlProcessor extends ExecutorProcessor<String> {
  private static final Curler CURLER = createCurler(
    FILTER_XML, CONVERT_RSQUOTE_HEX
  );

  private final ProcessorContext mContext;

  /**
   * Constructs an HTML processor capable of curling straight quotes.
   *
   * @param successor The next chained processor for text processing.
   */
  public HtmlProcessor(
    final Processor<String> successor,
    final ProcessorContext context ) {
    super( successor );

    mContext = context;
  }

  /**
   * Returns the given string with quotations marks encoded as HTML entities,
   * provided the user opted to curl quotation marks.
   *
   * @param t The string having quotation marks to replace.
   * @return The string with quotation marks curled.
   */
  @Override
  public String apply( final String t ) {
    return mContext.getCurlQuotes() ? CURLER.apply( t ) : t;
  }
}
