/* Copyright 2020-2021 White Magic Software, Ltd. -- All rights reserved. */
package com.keenwrite.processors.html;

import com.keenwrite.preview.HtmlPreview;
import com.keenwrite.processors.ExecutorProcessor;
import com.keenwrite.processors.ProcessorContext;
import com.whitemagicsoftware.keenquotes.parser.Curler;

import static com.keenwrite.processors.html.Configuration.createCurler;
import static com.whitemagicsoftware.keenquotes.lex.FilterType.FILTER_XML;
import static com.whitemagicsoftware.keenquotes.parser.Apostrophe.CONVERT_RSQUOTE;

/**
 * Responsible for notifying the {@link HtmlPreview} when the succession
 * chain has updated. This decouples knowledge of changes to the editor panel
 * from the HTML preview panel as well as any processing that takes place
 * before the final HTML preview is rendered. This is the last link in the
 * processor chain.
 */
public final class HtmlPreviewProcessor extends ExecutorProcessor<String> {
  private static final Curler CURLER = createCurler(
    FILTER_XML, CONVERT_RSQUOTE
  );

  /**
   * Allows quote curling in the preview panel.
   */
  private final ProcessorContext mContext;

  /**
   * There is only one preview panel.
   */
  private static HtmlPreview sPreview;

  /**
   * Constructs the end of a processing chain.
   *
   * @param context Typesetting options.
   * @param preview The pane to update with the post-processed document.
   */
  public HtmlPreviewProcessor(
    final ProcessorContext context,
    final HtmlPreview preview ) {
    mContext = context;
    sPreview = preview;
  }

  /**
   * Update the preview panel using HTML from the succession chain.
   *
   * @param html The document content to render in the preview pane. The HTML
   *             should not contain a doctype, head, or body tag.
   * @return The given {@code html} string.
   */
  @Override
  public String apply( final String html ) {
    assert html != null;
    final var result = mContext.getCurlQuotes() ? CURLER.apply( html ) : html;

    sPreview.render( result );
    return html;
  }
}
