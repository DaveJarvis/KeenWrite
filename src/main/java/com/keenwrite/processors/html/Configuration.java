package com.keenwrite.processors.html;

import com.whitemagicsoftware.keenquotes.lex.FilterType;
import com.whitemagicsoftware.keenquotes.parser.Apostrophe;
import com.whitemagicsoftware.keenquotes.parser.Contractions;
import com.whitemagicsoftware.keenquotes.parser.Curler;

import static com.whitemagicsoftware.keenquotes.parser.Contractions.*;

/**
 * Ensures the contractions aren't created multiple times when creating
 * a class that changes typographic straight quotes into curly quotes.
 */
public final class Configuration {
  /**
   * Creates contracts with a custom set of unambiguous English contractions.
   */
  private final static Contractions CONTRACTIONS = new Builder().build();

  public static Curler createCurler(
    final FilterType filterType,
    final Apostrophe apostrophe ) {
    return new Curler( CONTRACTIONS, filterType, apostrophe );
  }
}
