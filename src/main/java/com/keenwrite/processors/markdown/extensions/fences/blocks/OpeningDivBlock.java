/* Copyright 2020-2021 White Magic Software, Ltd. -- All rights reserved. */
package com.keenwrite.processors.markdown.extensions.fences.blocks;

import com.vladsch.flexmark.html.HtmlWriter;
import com.vladsch.flexmark.util.html.Attribute;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.util.ArrayList;
import java.util.List;

/**
 * Responsible for helping to generate an opening {@code div} element.
 */
public final class OpeningDivBlock extends DivBlock {
  private final List<Attribute> mAttributes = new ArrayList<>();

  public OpeningDivBlock( final BasedSequence chars,
                          final List<Attribute> attrs ) {
    super( chars );
    assert attrs != null;
    mAttributes.addAll( attrs );
  }

  @Override
  public void write( final HtmlWriter html ) {
    mAttributes.forEach( html::attr );
    html.withAttr().tag( HTML_DIV );
  }
}
