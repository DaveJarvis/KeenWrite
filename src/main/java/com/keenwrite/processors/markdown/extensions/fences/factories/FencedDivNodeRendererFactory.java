/* Copyright 2020-2021 White Magic Software, Ltd. -- All rights reserved. */
package com.keenwrite.processors.markdown.extensions.fences.factories;

import com.keenwrite.processors.markdown.extensions.common.MarkdownNodeRendererFactory;
import com.keenwrite.processors.markdown.extensions.fences.renderers.FencedDivRenderer;
import com.vladsch.flexmark.html.renderer.NodeRenderer;
import com.vladsch.flexmark.util.data.DataHolder;

public class FencedDivNodeRendererFactory extends MarkdownNodeRendererFactory {
  @Override
  protected NodeRenderer createNodeRenderer( final DataHolder options ) {
    return new FencedDivRenderer();
  }
}
