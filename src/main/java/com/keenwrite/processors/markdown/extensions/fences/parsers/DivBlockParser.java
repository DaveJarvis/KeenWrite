/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.parsers;

import com.vladsch.flexmark.parser.block.AbstractBlockParser;
import com.vladsch.flexmark.parser.block.BlockContinue;
import com.vladsch.flexmark.parser.block.ParserState;

/**
 * Abstracts common {@link OpeningParser} and
 * {@link ClosingParser} methods.
 */
public abstract class DivBlockParser extends AbstractBlockParser {
  @Override
  public BlockContinue tryContinue( final ParserState state ) {
    return BlockContinue.none();
  }

  @Override
  public void closeBlock( final ParserState state ) {}
}
