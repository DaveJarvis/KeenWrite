/* Copyright 2020-2021 White Magic Software, Ltd. -- All rights reserved. */
package com.keenwrite.processors.markdown.extensions.fences.blocks;

import com.vladsch.flexmark.html.HtmlWriter;
import com.vladsch.flexmark.util.sequence.BasedSequence;

/**
 * Responsible for helping to generate a closing {@code div} element.
 */
public final class ClosingDivBlock extends DivBlock {
  public ClosingDivBlock( final BasedSequence chars ) {
    super( chars );
  }

  @Override
  public void write( final HtmlWriter html ) {
    html.closeTag( HTML_DIV );
  }
}
