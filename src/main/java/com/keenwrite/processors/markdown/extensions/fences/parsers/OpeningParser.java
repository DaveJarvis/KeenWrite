/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.parsers;

import com.keenwrite.processors.markdown.extensions.fences.blocks.OpeningDivBlock;
import com.vladsch.flexmark.util.ast.Block;
import com.vladsch.flexmark.util.html.Attribute;
import com.vladsch.flexmark.util.html.AttributeImpl;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.util.regex.Pattern.UNICODE_CHARACTER_CLASS;
import static java.util.regex.Pattern.compile;

/**
 * Responsible for creating an instance of {@link OpeningDivBlock}.
 */
public class OpeningParser extends DivBlockParser {
  /**
   * Matches either individual CSS definitions (id/class, {@code <d>}) or
   * key/value pairs ({@code <k>} and {@link <v>}). The key/value pair
   * will match optional quotes.
   */
  private static final Pattern ATTR_PAIRS = compile(
    "\\s*" +
    "(?<d>[#.][\\p{Alnum}\\-_]+[^\\s=])|" +
    "((?<k>[\\p{Alnum}\\-_]+)=" +
    "\"*(?<v>(?<=\")[^\"]+(?=\")|(\\S+))\"*)",
    UNICODE_CHARACTER_CLASS );

  /**
   * Matches whether extended syntax is being used.
   */
  private static final Pattern ATTR_CSS = compile( "\\{(.+)}" );

  private final OpeningDivBlock mBlock;

  /**
   * Parses the arguments upon construction.
   *
   * @param args Text after :::, excluding leading/trailing whitespace.
   */
  public OpeningParser( final String args ) {
    final var attrs = new ArrayList<Attribute>();
    final var cssMatcher = ATTR_CSS.matcher( args );

    if( cssMatcher.matches() ) {
      // Split the text between braces into tokens and/or key-value pairs.
      final var pairMatcher =
        ATTR_PAIRS.matcher( cssMatcher.group( 1 ) );

      while( pairMatcher.find() ) {
        final var cssDef = pairMatcher.group( "d" );
        String cssAttrKey = "class";
        final String cssAttrVal;

        // When no regular CSS definition (id or class), use key/value pairs.
        if( cssDef == null ) {
          cssAttrKey = format( "data-%s", pairMatcher.group( "k" ) );
          cssAttrVal = pairMatcher.group( "v" );
        }
        else {
          // This will strip the "#" and "." off the start of CSS definition.
          var index = 1;

          // Default CSS attribute name is "class", switch to "id" for #.
          if( cssDef.startsWith( "#" ) ) {
            cssAttrKey = "id";
          }
          else if( !cssDef.startsWith( "." ) ) {
            index = 0;
          }

          cssAttrVal = cssDef.substring( index );
        }

        attrs.add( AttributeImpl.of( cssAttrKey, cssAttrVal ) );
      }
    }
    else {
      attrs.add( AttributeImpl.of( "class", args ) );
    }

    final var chars = BasedSequence.of( args );

    mBlock = new OpeningDivBlock( chars, attrs );
  }

  @Override
  public Block getBlock() {
    return mBlock;
  }
}
