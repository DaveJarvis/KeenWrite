/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.factories;

import com.keenwrite.processors.markdown.extensions.fences.parsers.ClosingParser;
import com.keenwrite.processors.markdown.extensions.fences.parsers.OpeningParser;
import com.vladsch.flexmark.parser.block.AbstractBlockParserFactory;
import com.vladsch.flexmark.parser.block.BlockStart;
import com.vladsch.flexmark.parser.block.MatchedBlockParser;
import com.vladsch.flexmark.parser.block.ParserState;
import com.vladsch.flexmark.util.data.DataHolder;

import static com.keenwrite.processors.markdown.extensions.fences.FencedDivExtension.FENCE;
import static com.keenwrite.processors.markdown.extensions.fences.FencedDivExtension.FENCE_OPENING;

/**
 * Responsible for creating a fenced div parser that is appropriate for the
 * type of fenced div encountered: opening or closing.
 */
public class DivBlockParserFactory extends AbstractBlockParserFactory {
  public DivBlockParserFactory( final DataHolder options ) {
    super( options );
  }

  /**
   * Try to match an opening or closing fenced div.
   *
   * @param state       Block parser state.
   * @param blockParser Last matched open block parser.
   * @return Wrapper for the opening or closing parser, upon finding :::.
   */
  @Override
  public BlockStart tryStart(
    final ParserState state, final MatchedBlockParser blockParser ) {
    return
      state.getIndent() == 0 && FENCE.matcher( state.getLine() ).matches()
        ? parseFence( state )
        : BlockStart.none();
  }

  /**
   * After finding a fenced div, this will further disambiguate an opening
   * from a closing fence.
   *
   * @param state Block parser state, contains line to parse.
   * @return Wrapper for the opening or closing parser, upon finding :::.
   */
  private BlockStart parseFence( final ParserState state ) {
    final var line = state.getLine();
    final var fence = FENCE_OPENING.matcher( line.trim() );

    return BlockStart.of(
      fence.matches()
        ? new OpeningParser( fence.group( 1 ) )
        : new ClosingParser( line )
    ).atIndex( state.getIndex() );
  }
}
