/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.quotes;

import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.html.HtmlWriter;
import com.vladsch.flexmark.html.renderer.NodeRenderer;
import com.vladsch.flexmark.html.renderer.NodeRendererContext;
import com.vladsch.flexmark.html.renderer.NodeRendererFactory;
import com.vladsch.flexmark.html.renderer.NodeRenderingHandler;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * Responsible for preventing escaped quotes from being converted to regular
 * quotes.
 */
public class EscapedQuoteNodeRenderer implements NodeRenderer {
  public static class Factory implements NodeRendererFactory {
    @Override
    public @NotNull NodeRenderer apply( @NotNull DataHolder options ) {
      return new EscapedQuoteNodeRenderer();
    }
  }

  @Override
  public Set<NodeRenderingHandler<?>> getNodeRenderingHandlers() {
    final var handlers = new HashSet<NodeRenderingHandler<?>>();

    handlers.add( new NodeRenderingHandler<>( Text.class, this::renderText ) );

    return handlers;
  }

  private void renderText(
    final Text node,
    final NodeRendererContext context,
    final HtmlWriter html ) {
    // By default, rendering will unescape escaped quotation marks. Overriding
    // how text is produced with the verbatim characters ensures the escape
    // sequence is retained (i.e., \").
    html.text( node.getChars().toString() );
  }
}
