/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.factories;

import com.keenwrite.processors.markdown.extensions.fences.processors.FencedDivParagraphPreProcessor;
import com.vladsch.flexmark.parser.block.ParagraphPreProcessor;
import com.vladsch.flexmark.parser.block.ParagraphPreProcessorFactory;
import com.vladsch.flexmark.parser.block.ParserState;
import com.vladsch.flexmark.parser.core.ReferencePreProcessorFactory;
import org.jetbrains.annotations.Nullable;

import java.util.Set;

public class FencedDivPreProcessorFactory
  implements ParagraphPreProcessorFactory {

  @Override
  public ParagraphPreProcessor apply( final ParserState state ) {
    return new FencedDivParagraphPreProcessor( state.getProperties() );
  }

  @Override
  public @Nullable Set<Class<?>> getBeforeDependents() {
    return Set.of();
  }

  @Override
  public @Nullable Set<Class<?>> getAfterDependents() {
    return Set.of( ReferencePreProcessorFactory.class );
  }

  @Override
  public boolean affectsGlobalScope() {
    return false;
  }

}
