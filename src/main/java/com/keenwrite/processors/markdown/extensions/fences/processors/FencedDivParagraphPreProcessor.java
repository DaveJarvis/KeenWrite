/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.processors;

import com.keenwrite.processors.markdown.extensions.fences.parsers.ClosingParser;
import com.keenwrite.processors.markdown.extensions.fences.parsers.OpeningParser;
import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.parser.block.ParagraphPreProcessor;
import com.vladsch.flexmark.parser.block.ParserState;
import com.vladsch.flexmark.util.data.MutableDataHolder;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.util.ArrayList;

import static com.keenwrite.processors.markdown.extensions.fences.FencedDivExtension.FENCE;
import static com.keenwrite.processors.markdown.extensions.fences.FencedDivExtension.FENCE_OPENING;

public class FencedDivParagraphPreProcessor
  implements ParagraphPreProcessor {
  public FencedDivParagraphPreProcessor( final MutableDataHolder unused ) {}

  @Override
  public int preProcessBlock( final Paragraph block,
                              final ParserState state ) {
    final var lines = block.getContentLines();

    // Stores lines matching opening or closing fenced div sigil.
    final var sigilLines = new ArrayList<BasedSequence>();

    for( final var line : lines ) {
      // Seeks a :::+ sigil.
      final var fence = FENCE.matcher( line );

      if( fence.find() ) {
        // Attributes after the fence are required to detect an open fence.
        final var attrs = FENCE_OPENING.matcher( line.trim() );
        final var match = attrs.matches();

        final var parser = match
          ? new OpeningParser( attrs.group( 1 ) )
          : new ClosingParser( line );
        final var divBlock = parser.getBlock();

        if( match ) {
          block.insertBefore( divBlock );
        }
        else {
          block.insertAfter( divBlock );
        }

        state.blockAdded( divBlock );

        // Schedule the line for removal (because it has been handled).
        sigilLines.add( line );
      }
    }

    sigilLines.forEach( lines::remove );

    return 0;
  }
}
