/* Copyright 2020-2024 White Magic Software, Ltd. -- All rights reserved. */
package com.keenwrite.processors.markdown.extensions.fences;

import com.keenwrite.processors.markdown.extensions.common.MarkdownParserExtension;
import com.keenwrite.processors.markdown.extensions.common.MarkdownRendererExtension;
import com.keenwrite.processors.markdown.extensions.fences.factories.CustomDivBlockParserFactory;
import com.keenwrite.processors.markdown.extensions.fences.factories.FencedDivNodeRendererFactory;
import com.keenwrite.processors.markdown.extensions.fences.factories.FencedDivPreProcessorFactory;
import com.vladsch.flexmark.html.renderer.NodeRendererFactory;
import com.vladsch.flexmark.parser.Parser.Builder;

import java.util.regex.Pattern;

import static java.util.regex.Pattern.UNICODE_CHARACTER_CLASS;
import static java.util.regex.Pattern.compile;

/**
 * Responsible for parsing div block syntax into HTML div tags. Fenced div
 * blocks start with three or more consecutive colons, followed by a space,
 * followed by attributes. The attributes can be either a single word, or
 * multiple words nested in braces. For example:
 *
 * <p>
 * ::: poem
 * Tyger Tyger, burning bright,
 * In the forests of the night;
 * What immortal hand or eye,
 * Could frame thy fearful symmetry?
 * :::
 * </p>
 * <p>
 * As well as:
 * </p>
 * <p>
 * ::: {#verse .p .d k=v author="Dickinson"}
 * Because I could not stop for Death --
 * He kindly stopped for me --
 * The Carriage held but just Ourselves --
 * And Immortality.
 * :::
 * </p>
 *
 * <p>
 * The second example produces the following starting {@code div} element:
 * </p>
 * {@literal <div id="verse" class="p d" data-k="v" data-author="Dickson">}
 *
 * <p>
 * This will parse fenced divs embedded inside of blockquote environments.
 * </p>
 */
public class FencedDivExtension extends MarkdownRendererExtension
  implements MarkdownParserExtension {
  /**
   * Matches any number of colons at start of line. This will match both the
   * opening and closing fences, with any number of colons.
   */
  public static final Pattern FENCE = compile( "^:::+.*" );

  /**
   * After a fenced div is detected, this will match the opening fence.
   */
  public static final Pattern FENCE_OPENING = compile(
    "^:::+\\s+([\\p{Alnum}\\-_]+|\\{.+})\\s*$",
    UNICODE_CHARACTER_CLASS );

  public static FencedDivExtension create() {
    return new FencedDivExtension();
  }

  @Override
  public void extend( final Builder builder ) {
    builder.customBlockParserFactory( new CustomDivBlockParserFactory() );
    builder.paragraphPreProcessorFactory( new FencedDivPreProcessorFactory() );
  }

  @Override
  protected NodeRendererFactory createNodeRendererFactory() {
    return new FencedDivNodeRendererFactory();
  }
}
