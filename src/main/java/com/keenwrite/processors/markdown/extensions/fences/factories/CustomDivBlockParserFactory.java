/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.factories;

import com.keenwrite.processors.markdown.extensions.common.MarkdownCustomBlockParserFactory;
import com.vladsch.flexmark.parser.block.BlockParserFactory;
import com.vladsch.flexmark.util.data.DataHolder;

/**
 * Responsible for creating an instance of {@link DivBlockParserFactory}.
 */
public class CustomDivBlockParserFactory
  extends MarkdownCustomBlockParserFactory {
  @Override
  public BlockParserFactory createBlockParserFactory(
    final DataHolder options ) {
    return new DivBlockParserFactory( options );
  }
}
