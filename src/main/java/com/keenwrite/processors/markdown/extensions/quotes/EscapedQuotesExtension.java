/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.quotes;

import com.vladsch.flexmark.util.data.MutableDataHolder;
import org.jetbrains.annotations.NotNull;

import static com.keenwrite.processors.markdown.extensions.quotes.EscapedQuoteNodeRenderer.Factory;
import static com.vladsch.flexmark.html.HtmlRenderer.Builder;
import static com.vladsch.flexmark.html.HtmlRenderer.HtmlRendererExtension;

/**
 * Responsible for extending the Markdown library with the ability to
 * retain escaped characters, rather than unescape the text. This is needed
 * so that the character sequence {@code \"} can be interpreted as a straight
 * quotation mark when output into the final document.
 */
public class EscapedQuotesExtension
  implements HtmlRendererExtension {

  @Override
  public void rendererOptions( @NotNull final MutableDataHolder options ) {}

  @Override
  public void extend(
    final Builder builder,
    @NotNull final String rendererType ) {
    builder.nodeRendererFactory( new Factory() );
  }

  public static EscapedQuotesExtension create() {
    return new EscapedQuotesExtension();
  }
}
