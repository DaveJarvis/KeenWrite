/* Copyright 2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.processors.markdown.extensions.fences.parsers;

import com.keenwrite.processors.markdown.extensions.fences.blocks.ClosingDivBlock;
import com.vladsch.flexmark.util.ast.Block;
import com.vladsch.flexmark.util.sequence.BasedSequence;

/**
 * Responsible for creating an instance of {@link ClosingDivBlock}.
 */
public class ClosingParser extends DivBlockParser {
  private final ClosingDivBlock mBlock;

  public ClosingParser( final BasedSequence line ) {
    mBlock = new ClosingDivBlock( line );
  }

  @Override
  public Block getBlock() {
    return mBlock;
  }
}
