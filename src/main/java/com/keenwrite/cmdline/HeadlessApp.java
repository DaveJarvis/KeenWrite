/* Copyright 2020-2024 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.keenwrite.cmdline;

import com.keenwrite.AppCommands;
import com.keenwrite.events.StatusEvent;
import org.greenrobot.eventbus.Subscribe;

import java.io.PrintStream;

import static com.keenwrite.events.Bus.register;
import static java.lang.String.format;

/**
 * Responsible for running the application in headless mode.
 */
public class HeadlessApp {
  /**
   * Contains directives that control text file processing.
   */
  private final Arguments mArgs;

  /**
   * Where to write error messages.
   */
  private final PrintStream mErrStream;

  /**
   * Creates a new command-line version of the application.
   *
   * @param args      The post-processed command-line arguments.
   * @param errStream Where to write error messages.
   */
  public HeadlessApp( final Arguments args, final PrintStream errStream ) {
    assert args != null;

    mArgs = args;
    mErrStream = errStream;

    register( this );
  }

  /**
   * When a status message is shown, write it to the console, if not in
   * quiet mode.
   *
   * @param event The event published when the status changes.
   */
  @Subscribe
  public void handle( final StatusEvent event ) {
    assert event != null;

    if( !mArgs.quiet() ) {
      final var stacktrace = event.getProblem();
      final var problem = stacktrace.isBlank()
        ? ""
        : format( "%n%s", stacktrace );
      final var msg = format( "%s%s", event, problem );

      mErrStream.println( msg );
    }
  }

  private void run() {
    AppCommands.run( mArgs );
  }

  /**
   * Entry point for running the application in headless mode.
   *
   * @param args      The parsed command-line arguments.
   * @param errStream Where to write error messages.
   */
  public static void run(
    final Arguments args,
    final PrintStream errStream ) {
    final var app = new HeadlessApp( args, errStream );
    app.run();
  }
}
